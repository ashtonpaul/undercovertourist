"""undercovertourist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from apps.products.views import ProductList, ProductDetail
from apps.purchases.views import purchase_view, success_view

urlpatterns = [
    url(r'^$', ProductList.as_view(), name='product-list'),

    url(r'^product-detail/(?P<pk>(\d+))/$',
        ProductDetail.as_view(), name='product-detail'),

    url(r'^product-detail/(?P<pk>(\d+))/purchase/$',
        purchase_view, name='purchase-product'),

    url(r'^success/$', success_view, name='success'),

    url(r'^admin/', admin.site.urls),
]
