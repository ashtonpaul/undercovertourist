## Undercover Tourist Assignment
My submission to Undercover Tourist assignment.

Create a Web App consisting of four (4) customer facing views:
* Product Listing
* Product Detail
* Product Purchase
* Purchase Complete


## Cache
Leverage Django low-level cache functions to circumvent assignment functional requirements of availability and slow response times.

If project is being built for the first time don't forget to run:
`python manage.py createcachetable`


## Testing
Testing was augmented using python package *mock* and mock helpers.


## API Consumption
API consumption was done using python package *requests*


## Layout
Styling provided by CSS from Bootstrap.