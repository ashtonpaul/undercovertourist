# Dummy product and purchase responses used for mock responses
product = {
    "id": 1,
    "name": "ab",
    "price": 1,
    "inventory_on_hand": 100
}

product_no_inventory = {
    "id": 2,
    "inventory_on_hand": 0
}

product_list = [
    {"id": 1, "name": 'one'},
    {"id": 2, "name": 'two'}
]

purchase_result = {'confirmation_code': '123456'}


class MockResponse:
    """
    Response class for testing fake API calls
    """
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


def api_not_available(*args, **kwargs):
    """
    Mock API unavailable (midnight - 6am)
    """
    response = MockResponse(None, 500)
    return {"products": response.json()}


def api_working(*args, **kwargs):
    """
    Mock API working normal
    """
    response = MockResponse(product_list, 200)
    return {"products": response.json()}


def cache_working(*args, **kwargs):
    """
    Mock cache system works
    """
    response = MockResponse(product_list, 200)
    return response.json()


def mocked_product(self, *args, **kwargs):
    """
    Helper function for mocked product
    """
    return MockResponse(product, 200)


def mocked_purchase(self, *args, **kwargs):
    """
    Helper function for mocked product
    """
    return MockResponse(purchase_result, 200)


def mocked_no_inventory(self, *args, **kwargs):
    """
    Helper function for product with no inventory
    """
    return MockResponse(product_no_inventory, 200)
