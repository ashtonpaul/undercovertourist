from datetime import date

from django.conf import settings
from django.core.cache import cache


def set_cache(response):
    """
    Create new cached version and set timestamp
    """
    # Only set a non-null response
    if response:
        cache.set(settings.UT_CACHE_KEY, response, timeout=None)
        cache.set(settings.UT_CACHE_TIMESTAMP, date.today(), timeout=None)


def delete_cache():
    """
    Remove old cached version and timestamp
    """
    cache.delete(settings.UT_CACHE_KEY)
    cache.delete(settings.UT_CACHE_TIMESTAMP)
