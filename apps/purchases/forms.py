from django import forms

from models import PurchaseHistory


class PurchaseForm(forms.ModelForm):
    """
    Purchase Product Model Form
    """
    quantity = forms.IntegerField(min_value=1)

    class Meta:
        model = PurchaseHistory
        fields = ('customer_name', 'customer_email', 'customer_phone')

    def __init__(self, *args, **kwargs):
        """
        Set max value for quantity to inventory_on_hand for product
        """
        max_quantity = kwargs.pop('max_quantity', None)
        super(PurchaseForm, self).__init__(*args, **kwargs)
        self.fields['quantity'] = forms.IntegerField(min_value=1,
                                                     max_value=max_quantity)
