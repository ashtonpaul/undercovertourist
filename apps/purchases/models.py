from django.db import models
from django.core.validators import MinValueValidator


class PurchaseHistory(models.Model):
    """
    Store purchase data for each purchase
    """
    confirmation_code = models.CharField(max_length=10)
    quantity = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    product_name = models.CharField(max_length=10)
    product_price = models.FloatField()
    customer_name = models.CharField(max_length=80)
    customer_email = models.EmailField(max_length=100)
    customer_phone = models.CharField(max_length=15)

    class Meta:
        verbose_name = "Purchase History"
        verbose_name_plural = "Purchase History"

    def __str__(self):
        return '%s' % (self.confirmation_code)
