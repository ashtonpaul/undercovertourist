from django.contrib import admin

from models import PurchaseHistory


class PurchaseHistoryAdmin(admin.ModelAdmin):
    """
    Purchase History configuration in admin panel
    """
    list_display = (
        "confirmation_code",
        "quantity",
        "product_name",
        "product_price",
        "customer_name",
        "customer_email",
        "customer_phone",
    )

    search_fields = (
        'confirmation_code',
        'customer_name',
        'product_name',
    )

    readonly_fields = (
        'product_name',
        'product_price',
    )

    fieldsets = (
        (
         'Customer  Information', {
            'fields': (
                'customer_name',
                'customer_email',
                'customer_phone',)
            },
        ),
        (
         'Product Information', {
            'fields': (
                'confirmation_code',
                'quantity',
                'product_name',
                'product_price',)
            },
        ),
    )

admin.site.register(PurchaseHistory, PurchaseHistoryAdmin)
