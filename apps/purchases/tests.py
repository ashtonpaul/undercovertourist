import mock

from django.test import TestCase
from django.core.urlresolvers import reverse

from apps import mock_helper

from models import PurchaseHistory


class PurchaseTestCase(TestCase):
    """
    Test for purchases views
    """
    @mock.patch('requests.get', side_effect=mock_helper.mocked_no_inventory)
    def test_no_inventory_purchase(self, mock_get):
        """
        Redirect to product detail page if inventory is 0
        """
        response = self.client.get(reverse('purchase-product', args=[2]))
        self.assertNotEquals(response.request['PATH_INFO'],
                             response['Location'])

    @mock.patch('requests.get', side_effect=mock_helper.mocked_product)
    @mock.patch('requests.post', side_effect=mock_helper.mocked_purchase)
    def test_purchase_form(self, mock_get, mock_post):
        """
        Test that form validates good data
        """
        # get total before purchase submitted
        total = PurchaseHistory.objects.all().count()

        form_data = {
            'customer_name': 'Ashton Paul',
            'customer_email': 'ashton@ashtonpaul.com',
            'customer_phone': '888-888-8888',
            'quantity': '1'
        }

        # sumbit form
        response = self.client.post(
            reverse('purchase-product', args=[1]),
            form_data
        )

        # get new total after form submitted
        new_total = PurchaseHistory.objects.all().count()
        entry = PurchaseHistory.objects.get()

        self.assertEquals(total, 0)
        self.assertEquals(new_total, 1)
        self.assertEquals(entry.product_name, "ab")
        self.assertEquals(entry.customer_name, "Ashton Paul")
        self.assertEquals(entry.confirmation_code, '123456')
        self.assertEquals(entry.quantity, 1)
        self.assertNotEquals(total, new_total)
        self.assertTemplateUsed(response, 'success.html')
