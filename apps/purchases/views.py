from django.conf import settings
from django.shortcuts import render, redirect

from apps.core import query_api
from apps.products import services

from models import PurchaseHistory
from forms import PurchaseForm


def purchase_view(request, pk):
    """
    Form handling view to recieve customer info and send result to API
    """
    # Retreive the product details
    product = services.get_product_detail(pk)
    inventory_on_hand = product['inventory_on_hand']

    # Only shwo for products with at least 1 in inventory
    if inventory_on_hand > 0:

        if request.method == 'POST':
            form = PurchaseForm(request.POST, max_quantity=inventory_on_hand)

            if form.is_valid():
                # Submit purchase order to API
                url = settings.UT_PRODUCTS + str(product['id']) + '/purchase/'
                payload = {
                    'customer_name': form.cleaned_data['customer_name'],
                    'customer_email': form.cleaned_data['customer_email'],
                    'customer_phone': form.cleaned_data['customer_phone'],
                    'quantity': form.cleaned_data['quantity']
                }
                response = query_api('post', url, payload)

                # Save the purchase to db if status code 200
                if response.status_code == 200:
                    data = response.data
                    form_data = form.cleaned_data
                    entry = PurchaseHistory(
                                confirmation_code=data['confirmation_code'],
                                quantity=form_data['quantity'],
                                product_name=product['name'],
                                product_price=product['price'],
                                customer_name=form_data['customer_name'],
                                customer_email=form_data['customer_email'],
                                customer_phone=form_data['customer_phone'],
                            )
                    entry.save()

                # Confirmation view
                return success_view(
                    request,
                    status_code=response.status_code,
                    name=form_data['customer_name'],
                    product=product['name'],
                    confirmation_code=data['confirmation_code']
                )
        else:
            form = PurchaseForm(max_quantity=inventory_on_hand)

        context = {'form': form, 'product': product}
        return render(request, 'purchases.html', context)

    # If purchase view acccess product with no inventory
    else:
        return redirect('product-detail', pk=product['id'])


def success_view(request, status_code=0, name="",
                 product="", confirmation_code=0):
    """
    Confirmation view that purchase was complete or not
    """
    if status_code == 200:
        context = {
            'name': name,
            'product': product,
            'confirmation_code': confirmation_code
        }
    else:
        message = "Sorry, something went wrong with the order. " \
                  "Please try to purchase again"
        context = {'message': message}

    return render(request, 'success.html', context)
