import requests

from django.conf import settings


class QueryResponse(object):
    """
    Custom API query response object
    """
    def __init__(self):
        self.data = None

    def __repr__(self):
        """
        If data field available return the value
        """
        return '{}'.format(self.data)

    def __str__(self):
        """
        Return the same as __repr__
        """
        return self.__repr__()

    def __getitem__(self, item):
        return item


def query_api(method, url, data={}):
    """
    Helper function that accesses API endpoints
    """
    headers = {'X-AUTH': settings.UT_AUTH_INFO}

    if method.upper() == 'GET':
        request = requests.get(url, headers=headers)
    elif method.upper() == 'POST':
        request = requests.post(url, headers=headers, data=data)

    # Save data along with status code
    response = QueryResponse()
    response.status_code = request.status_code
    response.data = request.json()

    return response
