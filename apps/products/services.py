from datetime import date

from django.core.cache import cache
from django.conf import settings

from apps.core import query_api
from apps.cache import set_cache, delete_cache


def retrieve_list():
    """
    Get the json data from API endpoint and return products json
    """
    try:
        response = query_api('get', settings.UT_PRODUCTS)
        response = response.data['results']

    # Catch all errors (eg. timeout, availability issues, etc)
    except:
        response = None

    return response


def get_products():
    """
    Return response from cache if available or from API
    """
    # Test for 1st of month condition
    today = date.today()
    if today.day == settings.UT_UPDATE_DAY:

        # Get the timestamped month of last cached results
        cache_date = cache.get(settings.UT_CACHE_TIMESTAMP, None)

        # If the cache hasn't been already updated for the month
        if cache_date and cache_date.month != today.month:

            # If API service avaiable purge and update
            response = retrieve_list()
            if response:
                delete_cache()
                set_cache(response)

    # Use cache if present else cache new response
    cached_response = cache.get(settings.UT_CACHE_KEY, None)
    if cached_response:
        response = cached_response
    else:
        response = retrieve_list()
        set_cache(response)

    return {'products': response}


def get_product_detail(product_id):
    """
    Return json response for individual product
    """
    response = query_api('get', settings.UT_PRODUCTS + product_id + '/')
    return response.data
