from django.shortcuts import render
from django.views.generic import TemplateView, DetailView

import services


class ProductList(TemplateView):
    """
    Displays a list of products along with pricing information
    """
    def get(self, request):
        """
        Get list of product items
        """
        context = services.get_products()
        return render(request, 'products.html', context)


class ProductDetail(DetailView):
    """
    Displays the details of the product along with a purchase option
    """
    def get(self, request, pk):
        """
        Retrieve produt details from API
        """
        product = services.get_product_detail(pk)
        context = {'product': product}
        return render(request, 'product_detail.html', context)
