import mock

from django.test import TestCase
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.cache import cache

from apps import mock_helper


class ProductListTestCase(TestCase):
    """
    Test for product listings view
    """
    @mock.patch(
        'apps.products.services.get_products',
        side_effect=mock_helper.api_working
    )
    def test_get_list(self, mock_get):
        """
        Test if lists work and display on page
        """
        response = self.client.get(reverse('product-list'))
        self.assertEqual(response.status_code, 200)
        self.assertIn({"id": 1, "name": 'one'}, response.context['products'])

    @mock.patch(
        'apps.products.services.retrieve_list',
        side_effect=mock_helper.cache_working
    )
    def test_cached_response(self, mock_get):
        """
        Test cache response and availability
        """
        pre_get_list = cache.get(settings.UT_CACHE_KEY)
        response = self.client.get(reverse('product-list'))
        post_get_list = cache.get(settings.UT_CACHE_KEY)
        cached_response = self.client.get(reverse('product-list'))

        self.assertNotEqual(pre_get_list, post_get_list)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['products'])
        self.assertEqual(cached_response.status_code, 200)
        self.assertTrue(cached_response.context['products'])
        self.assertEqual(
            response.context['products'],
            cached_response.context['products']
        )

    @mock.patch(
        'apps.products.services.get_products',
        side_effect=mock_helper.api_not_available
    )
    def test_api_unavailable(self, mock_get):
        """
        If the API is unreachable and no cache is present
        """
        response = self.client.get(reverse('product-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['products'], None)
        self.assertContains(response, 'UT_ERROR')


class ProductDetailTestCase(TestCase):
    """
    Test product detail view
    """
    @mock.patch('requests.get', side_effect=mock_helper.mocked_product)
    def test_product_detail(self, mock_get):
        """
        Test that product details show with purchase option
        """
        response = self.client.get(reverse('product-detail',
                                   args=[mock_helper.product["id"]]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'name="purchase-button"')

    @mock.patch('requests.get', side_effect=mock_helper.mocked_no_inventory)
    def test_no_inventory(self, mock_get):
        """
        Use a mocked response to get result of no inventory product
        """
        response = self.client.get(reverse('product-detail',
                                   args=[
                                         mock_helper.product_no_inventory['id']
                                   ]))

        inventory = response.context['product']['inventory_on_hand']

        self.assertEqual(response.status_code, 200)
        self.assertEqual(inventory, 0)
        self.assertNotContains(response, 'name="purchase-button"')
